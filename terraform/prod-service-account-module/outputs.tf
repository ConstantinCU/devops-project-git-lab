output "k8s_service_account_email" {
  value       = google_service_account.k8s_service_account.email
  description = "Email du service account de kubernetes"
}
