### SERVICE ACCOUNT ###

# Création du service account pour kubernetes
resource "google_service_account" "k8s_service_account" {
  project      = var.project_id
  account_id   = "k8s-iam"
  display_name = "k8s-service-account"
  description  = "K8s Service Account"
}

# # # Clé du service account
# # resource "google_service_account_key" "kubernetes_service_account_key" {
# #   service_account_id = google_service_account.kubernetes_service_account.name
# #   public_key_type    = "TYPE_X509_PEM_FILE"
# # }

# # # Import du credentials.json
# # resource "local_file" "kubernetes_service_account_key_file" {
# #   content  = google_service_account_key.kubernetes_service_account_key.private_key
# #   filename = "credentials_prod.json"
# # }

# Attribution de rôles pour le projet
resource "google_project_iam_binding" "project_service_account_roles" {
  project = var.project_id
  role    = "roles/editor"

  members = [
    "serviceAccount:${google_service_account.k8s_service_account.email}",
    "serviceAccount:${var.cloudservices_iam}",
  ]
}

### IAM ###

# resource "google_project_iam_policy" "project" {
#   project     = var.project_id
#   policy_data = data.google_iam_policy.cloud_services.policy_data
# }

# data "google_iam_policy" "cloud_services" {
#   binding {
#     role = "roles/editor"

#     members = [
#       "user:477921717275@cloudservices.gserviceaccount.com",
#     ]
#   }
# }
