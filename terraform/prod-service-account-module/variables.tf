variable "project_id" {
  type        = string
  description = "ID du projet utilisé"
}

variable "cloudservices_iam" {
  type        = string
  description = "Iam cloud services"
}
