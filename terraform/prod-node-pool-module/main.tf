### NODE POOL ###

# Node pool general
resource "google_container_node_pool" "general" {
  name       = "general"
  location   = var.zone
  cluster    = var.prod_app_cluster_id
  project    = var.project_id
  node_count = 1

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    preemptible  = false
    machine_type = var.machine_type
    disk_size_gb = 20

    labels = {
      role = "general"
    }

    service_account = var.k8s_service_account_email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}
