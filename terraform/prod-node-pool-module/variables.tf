variable "machine_type" {
  type        = string
  description = "Type de machines utilisées pour les cluster dans l'environnement prod"
}

variable "prod_app_cluster_id" {
  type        = string
  description = "ID du cluster app prod"
}

variable "k8s_service_account_email" {
  type        = string
  description = "Email du service account de kubernetes"
}

variable "region" {
  type        = string
  description = "Region utilisé dans GCP"
}

variable "zone" {
  type        = string
  description = "Zone utilisée dans GCP"
}

variable "project_id" {
  type        = string
  description = "ID du projet utilisé"
}
