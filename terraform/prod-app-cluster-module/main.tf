### CLUSTER  ###

# Création du cluster
resource "google_container_cluster" "prod_app_cluster" {
  name                     = "prod-app-cluster"
  project                  = var.project_id
  location                 = var.zone
  # node_locations           = [var.second_zone]
  networking_mode          = "VPC_NATIVE"
  network                  = var.network_id_prod
  subnetwork               = var.subnetwork_id_prod
  initial_node_count       = 1
  remove_default_node_pool = true
  deletion_protection      = false
  logging_service          = "logging.googleapis.com/kubernetes"
  monitoring_service       = "monitoring.googleapis.com/kubernetes"

  release_channel {
    channel = "REGULAR"
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "k8s-pod-range"
    services_secondary_range_name = "k8s-service-range"
  }

  network_policy {
    provider = "PROVIDER_UNSPECIFIED"
    enabled  = true
  }
}
