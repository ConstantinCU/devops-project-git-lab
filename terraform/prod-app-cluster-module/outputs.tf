output "prod_app_cluster_id" {
  value       = google_container_cluster.prod_app_cluster.id
  description = "ID du cluster app prod"
}
