variable "zone" {
  type        = string
  description = "Zone utilisée dans GCP"
}

variable "second_zone" {
  type        = string
  description = "Deuxième zone pouvant être utilisée dans GCP"
}

variable "network_id_prod" {
  type        = string
  description = "ID du réseau prod"
}

variable "subnetwork_id_prod" {
  type        = string
  description = "ID du sous-réseau prod"
}

variable "image_os" {
  type        = string
  description = "OS utilisé dans les cluster"
}

variable "user" {
  type        = string
  description = "Utilisateur du compte GCP"
}

variable "machine_type" {
  type        = string
  description = "Type de machines utilisées pour les cluster dans l'environnement prod"
}

# PROJETS
variable "project_id" {
  type        = string
  description = "ID du projet utilisé"
}
