# ### ROUTER ###

# # Router
# resource "google_compute_router" "router_prod" {
#   name    = "router-prod"
#   region  = "us-east1"
#   project = var.project_id
#   network = var.network_self_link_prod
# }

# # Nat
# resource "google_compute_router_nat" "nat_prod" {
#   name                               = "nat-prod"
#   project                            = var.project_id
#   region                             = var.region
#   router                             = google_compute_router.router_prod.name
#   nat_ip_allocate_option             = "AUTO_ONLY"
#   source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

#   depends_on = [var.subnetwork_id_prod]
# }
