### VPC ###

# Réseau pour prod
resource "google_compute_network" "network_prod" {
  name                    = "network-prod"
  project                 = var.project_id
  routing_mode            = "REGIONAL"
  auto_create_subnetworks = false
}

# Sous-réseau pour prod
resource "google_compute_subnetwork" "subnetwork_prod" {
  name                     = "subnetwork-prod"
  project                  = var.project_id
  ip_cidr_range            = var.ip_cidr_range
  region                   = var.region
  network                  = google_compute_network.network_prod.id
  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "k8s-pod-range"
    ip_cidr_range = var.ip_cidr_range_pods
  }

  secondary_ip_range {
    range_name    = "k8s-service-range"
    ip_cidr_range = var.ip_cidr_range_services
  }
}
