output "network_id_prod" {
  value       = google_compute_network.network_prod.id
  description = "ID du réseau prod"
}

output "subnetwork_id_prod" {
  value       = google_compute_subnetwork.subnetwork_prod.id
  description = "ID du sous-réseau prod"
}

output "subnetwork_region_name" {
  value       = google_compute_subnetwork.subnetwork_prod.name
  description = "Region du subnetwork prod"
}

output "subnetwork_name" {
  value       = google_compute_subnetwork.subnetwork_prod.name
  description = "Nom du subnetwork prod"
}

output "network_self_link_prod" {
  value       = google_compute_network.network_prod.self_link
  description = "Lien vers le réseau prod"
}
