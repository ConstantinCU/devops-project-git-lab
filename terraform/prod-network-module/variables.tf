variable "ip_cidr_range" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau"
}

variable "ip_cidr_range_pods" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau pour les pods"
}

variable "ip_cidr_range_services" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau pour les services"
}

variable "region" {
  type        = string
  description = "Region utilisé dans GCP"
}

variable "project_id" {
  type        = string
  description = "ID du projet utilisé"
}