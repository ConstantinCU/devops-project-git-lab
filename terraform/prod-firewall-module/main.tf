### FIREWALL ###

# Firewall ssh
resource "google_compute_firewall" "firewall_ssh_prod" {
  name    = "allow-ssh-prod"
  network = var.network_self_link_prod
  project = var.project_id

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

# Firewall http https
resource "google_compute_firewall" "firewall_http_prod" {
  name    = "allow-http-https-prod"
  network = var.network_self_link_prod
  project = var.project_id

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  # target_tags   = ["gke-prod-app-cluster-18ba8068-node"]
}

# Firewall web server dev
resource "google_compute_firewall" "firewall_web_server_prod" {
  name    = "allow-web-server-prod"
  network = var.network_self_link_prod
  project = var.project_id

  allow {
    protocol = "tcp"
    ports    = ["4080"]
  }

  source_ranges = ["0.0.0.0/0"]
  # target_tags   = ["gke-prod-app-cluster-18ba8068-node"]
}

# Firewall database
resource "google_compute_firewall" "firewall_database_prod" {
  name    = "allow-database-prod"
  network = var.network_self_link_prod
  project = var.project_id

  allow {
    protocol = "tcp"
    ports    = ["3306"]
  }

  source_ranges = [var.ip_cidr_range]
  # target_tags   = ["gke-prod-app-cluster-18ba8068-node"]
}

# ### HEALTH CHECK ###
# resource "googgle_compute_firewall" "health" {
#   name    = "allow-health-check"
#   network = var.network_self_link_prod
#   project = var.project_id

#   allow {
#     protocol = "tcp"
#     ports    = ["3306"]
#   }

#   source_ranges = ["10.0.0.0/24"]

# }
