variable "network_self_link_prod" {
  type        = string
  description = "Lien vers le réseau prod"
}

variable "project_id" {
  type        = string
  description = "ID du projet utilisé"
}

variable "ip_cidr_range" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau"
}
