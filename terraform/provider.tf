### PROVIDER ###

# Version et backend
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.20.0"
    }
  }
  backend "gcs" {
    credentials = "credentials-host-c.json"
    bucket      = "prod-bucket-projet-app-devops-c"
    prefix      = "terraform/state"
  }
}

# Provider
provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project_id
  region      = var.region
}

