# PROJETS
variable "project_id" {
  type        = string
  description = "ID du projet utilisé"
}

variable "credentials_file" {
  type        = string
  description = "Credentials file principal"
}

variable "user" {
  type        = string
  description = "Utilisateur du compte GCP"
}

# PRINCIPAL

variable "region" {
  type        = string
  description = "Region utilisé dans GCP"
}

variable "zone" {
  type        = string
  description = "Zone utilisée dans GCP"
}

variable "second_zone" {
  type        = string
  description = "Deuxième zone pouvant être utilisée dans GCP"
}

variable "machine_type" {
  type        = string
  description = "Type de machines utilisées pour les cluster dans l'environnement prod"
}

variable "image_os" {
  type        = string
  description = "OS utilisé dans les cluster"
}

# IAM
variable "cloudservices_iam" {
  type        = string
  description = "Iam cloud services"
}

# NETWORK
variable "ip_cidr_range" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau"
}

variable "ip_cidr_range_pods" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau pour les pods"
}

variable "ip_cidr_range_services" {
  type        = string
  description = "Plage d'adresse IP utilisé dans le sous-réseau pour les services"
}
