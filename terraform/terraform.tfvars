# PROJET - modifiable
project_id       = "helical-loop-414420"
credentials_file = "credentials-host-c.json"
user             = "constantin.formation4"

# PRINCIPAL
region       = "europe-west6"
zone         = "europe-west6-b"
second_zone  = "europe-west6-c"
machine_type = "n2d-standard-2"
image_os     = "ubuntu-os-cloud/ubuntu-2004-lts"

# IAM
cloudservices_iam = "977880710937@cloudservices.gserviceaccount.com"

# NETWORK
ip_cidr_range          = "10.1.0.0/18"
ip_cidr_range_pods     = "10.48.0.0/14"
ip_cidr_range_services = "10.52.0.0/20"

