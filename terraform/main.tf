### MODULES (5) ###
# 1-prod_network
# 2-prod_firewall
# 3-prod_service_account
# 4-prod_node_pool
# 5-prod_app_cluster

# VPC
module "prod_network" {
  source                 = "./prod-network-module"
  project_id             = var.project_id
  ip_cidr_range          = var.ip_cidr_range
  ip_cidr_range_pods     = var.ip_cidr_range_pods
  ip_cidr_range_services = var.ip_cidr_range_services
  region                 = var.region
}

# FIREWALL
module "prod_firewall" {
  source                 = "./prod-firewall-module"
  network_self_link_prod = module.prod_network.network_self_link_prod
  project_id             = var.project_id
  ip_cidr_range          = var.ip_cidr_range
}

# SERVICE ACCOUNT
module "prod_service_account" {
  source            = "./prod-service-account-module"
  project_id        = var.project_id
  cloudservices_iam = var.cloudservices_iam
}

# NODE POOL POUR LE CLUSTER
module "prod_node_pool" {
  source                    = "./prod-node-pool-module"
  k8s_service_account_email = module.prod_service_account.k8s_service_account_email
  prod_app_cluster_id       = module.prod_app_cluster.prod_app_cluster_id
  project_id                = var.project_id
  machine_type              = var.machine_type
  region                    = var.region
  zone                      = var.zone
}

# APP CLUSTER
module "prod_app_cluster" {
  source             = "./prod-app-cluster-module"
  network_id_prod    = module.prod_network.network_id_prod
  subnetwork_id_prod = module.prod_network.subnetwork_id_prod
  project_id         = var.project_id
  zone               = var.zone
  second_zone        = var.second_zone
  machine_type       = var.machine_type
  image_os           = var.image_os
  user               = var.user
}
