#!/bin/bash

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 1/4 CONFIGURATION PROJET GCP ------ #
echo -e "\033[1;35m- Etape 1/4: Configuration projet GCP\033[0m"

echo "Configuration du projet GCP : $GCP_PROJECT_ID"
if gcloud config set project $GCP_PROJECT_ID; then
    echo -e "\033[0;32mLe projet GCP a été configuré avec succès : $GCP_PROJECT_ID\033[0m"
    echo "Vérification de la configuration du projet GCP :"
    gcloud config list
else
    echo -e "\033[0;31mErreur lors de la configuration du projet GCP.\033[0m"
    exit 1
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 2/4: CREATION BUCKET SI NON EXISTANT ------ #
echo -e "\033[1;35m- Etape 2/4: Création bucket si non existant\033[0m"

# Vérifier si le bucket existe
if gsutil ls -b gs://${GCP_BUCKET_DB}; then
    echo "Le bucket existe déjà : gs://${GCP_BUCKET_DB}"
else
    # Créer le bucket s'il n'existe pas
    gsutil mb -p ${GCP_PROJECT_ID} -c ${GCP_BUCKET_STORAGE_CLASS} -l ${GCP_BUCKET_REGION} gs://${GCP_BUCKET_DB}
    echo "Bucket créé : gs://${GCP_BUCKET_DB}"

    # Activer le versioning sur le bucket
    gsutil versioning set on gs://${GCP_BUCKET_DB}
    echo "Versioning activé sur le bucket : gs://${GCP_BUCKET_DB}"
fi