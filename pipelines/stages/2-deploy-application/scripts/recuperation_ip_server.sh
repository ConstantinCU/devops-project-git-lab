#!/bin/bash

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 1/2 RECUPERATION ADDRESSE IP SERVICE SERVER ------ #
echo -e "\033[1;35m- Etape 1/2: Récupération addresse ip service server\033[0m"

# Attendre que l'adresse IP du service server soit disponible
while true; do
    # Récupération de l'adresse IP externe du service server
    SERVER_IP=$(kubectl get service server -n $APP_NAMESPACE -o=jsonpath='{.status.loadBalancer.ingress[0].ip}')

    # Vérifier si l'adresse IP est vide
    if [ -z $SERVER_IP ]; then
        echo "L'adresse IP du service server n'a pas été trouvée. Attente de 10 secondes..."
        sleep 10
    else
        echo "Adresse IP du service server trouvée : $SERVER_IP"
        break
    fi
done

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 2/2: RAJOUT ADRESSE IP DANS FICHIER SERVER.JS COTE CLIENT ------ #
echo -e "\033[1;35m- Etape 2/2: Rajout adresse ip dans fichier server.js côté client\033[0m"

# Récupérer le nom du pod du client
CLIENT_POD=$(kubectl get pods -l app=client -n $APP_NAMESPACE -o jsonpath='{.items[0].metadata.name}')
echo "Voici le nom du pod du client : $CLIENT_POD"

# Vérifier si le pod client existe
if [ -z $CLIENT_POD ]; then
    echo "Le pod du client n'a pas été trouvé. Vérifiez votre configuration."
    exit 1
fi

# Exécuter une commande dans le pod pour modifier le fichier server.js
kubectl exec --namespace $APP_NAMESPACE $CLIENT_POD -- sed -i "s/IP_A_MODIFIER/$SERVER_IP/g" /client/src/api/server.js

# Afficher un message de confirmation
echo "L'adresse IP du serveur a été remplacée dans le fichier server.js du pod client."
