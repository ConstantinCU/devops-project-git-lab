#!/bin/bash

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 1/4 CONFIGURATION PROJET GCP ------ #
echo -e "\033[1;35m- Etape 1/4: Configuration projet GCP\033[0m"

# Permet d'activer les services concernés lorsque première utilisation du projet
gcloud services enable cloudresourcemanager.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable iam.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable compute.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable container.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable run.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable sourcerepo.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable dns.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable logging.googleapis.com --project=$GCP_PROJECT_ID
gcloud services enable monitoring.googleapis.com --project=$GCP_PROJECT_ID

# Configuration du projet GCP
echo "Configuration du projet GCP : $GCP_PROJECT_ID"
if gcloud config set project $GCP_PROJECT_ID; then
    echo -e "\033[0;32mLe projet GCP a été configuré avec succès : $GCP_PROJECT_ID\033[0m"
    echo "Vérification de la configuration du projet GCP :"
    gcloud config list
else
    echo -e "\033[0;31mErreur lors de la configuration du projet GCP.\033[0m"
    exit 1
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 2/4: CREATION BUCKET SI NON EXISTANT ------ #
echo -e "\033[1;35m- Etape 2/4: Création bucket si non existant\033[0m"

# Vérifier si le bucket existe
if gsutil ls -b gs://${GCP_BUCKET_NAME}; then
    echo "Le bucket existe déjà : gs://${GCP_BUCKET_NAME}"
else
    # Créer le bucket s'il n'existe pas
    gsutil mb -p ${GCP_PROJECT_ID} -c ${GCP_BUCKET_STORAGE_CLASS} -l ${GCP_BUCKET_REGION} gs://${GCP_BUCKET_NAME}
    echo "Bucket créé : gs://${GCP_BUCKET_NAME}"

    # Activer le versioning sur le bucket
    gsutil versioning set on gs://${GCP_BUCKET_NAME}
    echo "Versioning activé sur le bucket : gs://${GCP_BUCKET_NAME}"
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 3/4: VERIFICATION PRESENCE FICHIERS TERRAFORM ------ #
echo -e "\033[1;35m- Etape 3/4: Vérification présence fichiers terraform\033[0m"

# Vérification de la présence du dossier .terraform et du fichier .terraform.lock.hcl
if [ ! -d "$TERRAFORM_DIR/.terraform" ] || [ ! -f "$TERRAFORM_DIR/.terraform.lock.hcl" ]; then
    echo -e "\033[33mDossier .terraform ou fichier .terraform.lock.hcl introuvables. Exécution de 'terraform init'...\033[0m"
    cd $TERRAFORM_DIR
    terraform init -reconfigure
else
    echo -e "\033[32mDossier .terraform et fichier .terraform.lock.hcl déjà présents. Pas besoin d'exécuter 'terraform init'.\033[0m"
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 4/4: TERRAFORM APPLY ------ #
echo -e "\033[1;35m- Etape 4/4: Terraform apply\033[0m"
cd "$TERRAFORM_DIR"

# Exécuter la commande terraform plan et stocker la sortie dans une variable
PLAN_OUTPUT=$(terraform plan)

# Vérifier s'il y a des changements dans la sortie du plan
if echo "$PLAN_OUTPUT" | grep -q "No changes"; then
    echo -e "\033[32mAucun changement détecté. Pas besoin de lancer terraform apply.\033[0m"
else
    echo -e "\033[33mDes changements ont été détectés. Terraform apply en cours...\033[0m"
    # terraform destroy -auto-approve
    terraform apply -auto-approve
fi
